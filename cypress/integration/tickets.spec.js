const { it } = require("mocha");

describe ("Tickets", () =>{

    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    //Interação com Inputs
    it("Fills all the text input fields", () => {
        const firstName = "Rafael";
        const lastName = "Anderson";
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("aula_teste@MediaList.com");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    //Interação com Selects
    it("Select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    //Interação com Radio Buttons
    it("Select 'vip' tickets type", () => {
        cy.get("#vip").check();
    });

    //Interação com Check Box
    it("Selects 'social media' check box", () => {
        cy.get("#social-media").check();
    });

     //Interação com Check Box 02
    it("Selects 'friends', and 'publication', then uncheck 'friends'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    //First Test Assertion
    it("Has 'TICKETBOX' header's heading", () =>{
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("Alerts on invalid email", () =>{
        cy.get("#email")
        .as("email")
        .type("aula_teste-MediaList.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
        .clear()
        .type("aula_teste@MediaList.com");

        cy.get("#email.invalid").should("not.exist");
    });

    it("Fills and reset the form", () => {
        const firstName = "Rafael";
        const lastName = "Anderson";
        const fullName = `${firstName} ${lastName}`;
    
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("aula_teste@MediaList.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should("contain", 
        `I, ${fullName}, wish to buy 2 VIP tickets.`);

        cy.get("#agree").check();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");

    });

    it("Fills mandatory fields using support command", () => {
        const customer = {
            firstName: "Rafael",
            lastName: "Anderson",
            email: "aula_teste@MediaList.com"
        };

        cy.fillMandatoryField(customer);

        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");

    });
});